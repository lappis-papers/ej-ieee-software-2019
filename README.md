# ej-ieee-software-2019

## Preparing environment for Latex build

```console
apt install texlive-publishers
```

## Compiling pdf

```console
make
```
